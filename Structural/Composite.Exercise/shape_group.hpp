#ifndef SHAPE_GROUP_HPP_
#define SHAPE_GRO

#include <vector>
#include <memory>
#include <algorithm>
#include <boost/bind.hpp>
#include "shape.hpp"
#include "clone_factory.hpp"

//class Person
//{
//public:
//    Person(int id, const std::string& name) {}
//};


//void emplace_back_explained()
//{
//    std::vector<Person> people;

//    people.push_back(Person(1, "Jan"));
//    people.emplace_back(1, "Jan");
//}


namespace Drawing
{

// TO DO: zaimplementowac kompozyt grupuj�cy kszta�ty geometryczne
class ShapeGroup : public Shape
{
    std::vector<std::shared_ptr<Shape>> shapes_;

    // Shape interface
public:
    ShapeGroup() = default;

    ShapeGroup(const ShapeGroup& source)
    {
        for(const auto& s  : source.shapes_)
            shapes_.emplace_back(s->clone());
    }

    ShapeGroup& operator=(const ShapeGroup& source)
    {
        if (this != &source)
        {
            shapes_.clear();

            for(const auto& s  : source.shapes_)
                shapes_.emplace_back(s->clone());
        }

        return *this;
    }

    void draw() const
    {
        for(const auto& s : shapes_)
            s->draw();
    }

    void move(int dx, int dy)
    {
        for(const auto& s : shapes_)
            s->move(dx, dy);

        //std::for_each(shapes_.begin(), shapes_.end(), boost::bind(&Shape::move, _1, dx, dy));
    }

    void read(std::istream& in)
    {
        int count;
        in >> count;

        std::string type_identifier;

        for(int i = 0; i < count; ++i)
        {
            in >> type_identifier;
            Shape* shape = ShapeFactory::instance().create(type_identifier);
            shape->read(in);
            shapes_.emplace_back(shape);
        }
    }

    void write(std::ostream& out)
    {
        out << "ShapeGroup " << shapes_.size() << std::endl;

        for(const auto& s : shapes_)
            s->write(out);
    }

    Shape*clone() const
    {
        return new ShapeGroup(*this);
    }

    void add(std::shared_ptr<Shape> shape)
    {
        shapes_.push_back(shape);
    }
};

}

#endif /*SHAPE_GROUP_HPP_*/

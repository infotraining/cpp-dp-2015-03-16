#include "shape_group.hpp"
#include "clone_factory.hpp"

namespace
{
    using namespace Drawing;

    bool is_registred = ShapeFactory::instance()
            .register_shape("ShapeGroup", new ShapeGroup);
}

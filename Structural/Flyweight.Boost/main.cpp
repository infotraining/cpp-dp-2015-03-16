#include <iostream>
#include <vector>
#include <tuple>
#include <boost/flyweight.hpp>
#include <boost/algorithm/string.hpp>

using namespace std;

//struct FirstNamePool {};
//struct LastNamePool {};

class Platnik
{
    int id_;
    boost::flyweight<std::string> imie_;
    std::string nazwisko_;
public:
    Platnik(int id, const string& imie, const string& nazwisko)
        : id_(id), imie_(imie), nazwisko_(nazwisko)
    {}

    int id() const
    {
        return id_;
    }

    string imie() const
    {
        return imie_;
    }

    void set_imie(const string& imie)
    {
        imie_ = imie;
    }

    void to_upper()
    {
        imie_ = boost::to_upper_copy(imie_.get());
        boost::to_upper(nazwisko_);
    }

    string nazwisko() const
    {
        return nazwisko_;
    }

    void set_nazwisko(const string& nazwisko)
    {
        nazwisko_ = nazwisko;
    }

    bool operator==(const Platnik& p)
    {
        return std::tie(id_, imie_, nazwisko_) == std::tie(p.id_, p.imie_, p.nazwisko_);
    }
};


int main()
{
    vector<Platnik> platnicy { Platnik{1, "Jan", "Kowalski"}, Platnik{2, "Jan", "Nowak"} };

    platnicy.emplace_back(3, "Anna", "Kowalska");
    platnicy.emplace_back(1, "Anna", "Nowakowska");
    platnicy.insert(platnicy.end(), { Platnik{6, "Jan", "Nijaki"}, Platnik{10, "Anna", "Anonim"} });

    platnicy[0].to_upper();
    platnicy[1].to_upper();


    for(const auto& p : platnicy)
    {
        cout << p.imie() << " " << p.nazwisko() << endl;
    }
}


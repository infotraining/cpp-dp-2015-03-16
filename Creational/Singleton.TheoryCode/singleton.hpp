#ifndef SINGLETON_HPP_
#define SINGLETON_HPP_

#include <iostream>
#include <memory>
#include <mutex>
#include <atomic>

namespace Meyers
{
    class Singleton
    {
    public:
        Singleton(const Singleton&) = delete;
        Singleton& operator=(const Singleton&) = delete;

        static Singleton& instance()
        {
            static Singleton unique_instance;

            return unique_instance;
        }

        void do_something();

    private:
        Singleton() // uniemozliwienie klientom tworzenie nowych singletonow
        {
            std::cout << "Constructor of singleton" << std::endl;
        }

        ~Singleton() // prywatny destruktor chroni przed wywolaniem delete dla adresu instancji
        {
            std::cout << "Singleton has been destroyed!" << std::endl;
        }
    };

    void Singleton::do_something()
    {
        std::cout << "Singleton instance at " << std::hex << &instance() << std::endl;
    }
}

namespace SingletonWithAtomics
{
    class Singleton
    {
    public:
        Singleton(const Singleton&) = delete;
        Singleton& operator=(const Singleton&) = delete;

        ~Singleton() // prywatny destruktor chroni przed wywolaniem delete dla adresu instancji
        {
            std::cout << "Singleton has been destroyed!" << std::endl;
        }

        static Singleton& instance()
        {
            if (!instance_.load())
            {
                std::lock_guard<std::mutex> lk(mtx_);
                if (!instance_.load())
                {
                    Singleton* temp = new Singleton();
                    instance_.store(temp);
//                    //1
//                    void* raw_mem = ::operator new(sizeof(Singleton));

//                    //3
//                    instance_ = static_cast<Singleton*>(raw_mem);

//                    //2
//                    new(raw_mem)Singleton();
                }
            }

            return *instance_;
        }

        void do_something();

    private:
        static std::mutex mtx_;
        static std::atomic<Singleton*> instance_;

        Singleton() // uniemozliwienie klientom tworzenie nowych singletonow
        {
            std::cout << "Constructor of singleton" << std::endl;
        }
    };

    //std::unique_ptr<Singleton> Singleton::instance_ {nullptr};

    std::mutex Singleton::mtx_;
    std::atomic<Singleton*> Singleton::instance_ {};


    void Singleton::do_something()
    {
        std::cout << "Singleton instance at " << std::hex << &instance() << std::endl;
    }
}

namespace SingletonWithUniquePtr
{
    class Singleton
    {
    public:
        Singleton(const Singleton&) = delete;
        Singleton& operator=(const Singleton&) = delete;

        ~Singleton() // prywatny destruktor chroni przed wywolaniem delete dla adresu instancji
        {
            std::cout << "Singleton has been destroyed!" << std::endl;
        }

        static Singleton& instance()
        {
            std::call_once(init_flag_, []{ instance_.reset(new Singleton);});

            return *instance_;
        }

        void do_something();

    private:
        static std::once_flag init_flag_;
        static std::unique_ptr<Singleton> instance_;

        Singleton() // uniemozliwienie klientom tworzenie nowych singletonow
        {
            std::cout << "Constructor of singleton" << std::endl;
        }
    };

    std::once_flag Singleton::init_flag_;
    std::unique_ptr<Singleton> Singleton::instance_ {nullptr};


    void Singleton::do_something()
    {
        std::cout << "Singleton instance at " << std::hex << &instance() << std::endl;
    }
}


#endif /*SINGLETON_HPP_*/

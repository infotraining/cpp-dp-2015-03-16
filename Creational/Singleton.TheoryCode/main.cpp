#include <iostream>
#include "singleton.hpp"

using namespace std;

int main()
{
    cout << "START" << endl;

    SingletonWithUniquePtr::Singleton::instance().do_something();

    auto& singleObject = SingletonWithUniquePtr::Singleton::instance();
	singleObject.do_something();
}

#ifndef ABSTRACT_FACTORY_HPP_
#define ABSTRACT_FACTORY_HPP_

#include <iostream>
#include <string>
#include <stdexcept>
#include <memory>

class AbstractProductA;
class AbstractProductB;

// "AbstractFactory"
class AbstractFactory
{
public:
    virtual std::unique_ptr<AbstractProductA> create_product_A() = 0;
    virtual std::unique_ptr<AbstractProductB> create_product_B() = 0;
    virtual ~AbstractFactory() = default;
};

// "AbstractProductA"
class AbstractProductA
{
public:
	virtual std::string description() = 0;
    virtual ~AbstractProductA() = default;
};

// "AbstractProductB"
class AbstractProductB
{
public:
	virtual std::string description() = 0;
    virtual void interact(AbstractProductA& a) = 0;
    virtual ~AbstractProductB() = default;
};

// "ProductA1"
class ProductA1 : public AbstractProductA
{
public:
    std::string description() override
	{
		return std::string("ProductA1");
	}
};

// "ProductB1"
class ProductB1 : public AbstractProductB
{
public:
    std::string description() override
	{
		return std::string("ProductB1");
	}

    void interact(AbstractProductA& a) override
	{
        std::cout << description() << " interacts with "
                << a.description() << std::endl;
	}
};

// "ProductA2"
class ProductA2 : public AbstractProductA
{
public:
    std::string description() override
	{
		return std::string("ProductA2");
	}
};

// "ProductB2"
class ProductB2 : public AbstractProductB
{
public:
    std::string description() override
	{
		return std::string("ProductB2");
	}

    void interact(AbstractProductA& a) override
	{
        std::cout << description() << " interacts with "
                << a.description() << std::endl;
	}
};

// "ConcreteFactory1"
class ConcreteFactory1 : public AbstractFactory
{
public:
    std::unique_ptr<AbstractProductA> create_product_A() override
	{
        return std::unique_ptr<AbstractProductA>(new ProductA1());
	}

    std::unique_ptr<AbstractProductB> create_product_B() override
	{
        return std::unique_ptr<AbstractProductB>(new ProductB1());
	}
};

// "ConcreteFactory2"
class ConcreteFactory2 : public AbstractFactory
{
public:
    std::unique_ptr<AbstractProductA> create_product_A() override
	{
        return std::unique_ptr<AbstractProductA>(new ProductA2());
	}

    std::unique_ptr<AbstractProductB> create_product_B() override
	{
        return std::unique_ptr<AbstractProductB>(new ProductB2());
	}
};

// "Client"
class Client
{
private:
    std::unique_ptr<AbstractProductA> productA_;
    std::unique_ptr<AbstractProductB> productB_;
public:
    Client(AbstractFactory& factory) :
        productA_(factory.create_product_A()), productB_(factory.create_product_B())
	{
	}

    Client(const Client&) = delete;
    Client& operator=(const Client&) = delete;

    void run()
	{
        productB_->interact(*productA_);
	}
};

#endif /*ABSTRACT_FACTORY_HPP_*/

#ifndef FACTORY_HPP_
#define FACTORY_HPP_

#include "shape.hpp"
#include <unordered_map>
#include <stdexcept>
#include <functional>

namespace GenericFactory
{

	template
	<
		typename AbstractProduct,
        typename IdentifierType = std::string,
        typename CreatorType = std::function<AbstractProduct* ()>
	>
	class Factory
	{
	private:
        typedef std::unordered_map<IdentifierType, CreatorType> CreatorsMapType;
	public:
		bool register_creator(const IdentifierType& id, CreatorType creator)
		{
			return creators_.insert(std::make_pair(id, creator)).second;
		}

		bool unregister_creator(const IdentifierType& id)
		{
			return creators_.erase(id) == 1;
		}

		AbstractProduct* create_object(const IdentifierType& id)
		{
            try
            {
                auto& creator = creators_.at(id);

                return creator();
            }
            catch(std::out_of_range& e)
            {
                throw std::runtime_error((std::string("Unknown Type ID: ") + id));
            }
		}

	private:
		CreatorsMapType creators_;
	};

}

#endif /* FACTORY_HPP_ */

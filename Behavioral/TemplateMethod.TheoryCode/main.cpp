#include "template_method.hpp"

using namespace std;

void client(AbstractClass& ac)
{
    ac.template_method();
}

int main()
{
	AbstractClass* c = new ConcreteClassA;
    client(*c);

	delete c;

	c = NULL;

	c = new ConcreteClassB();
    client(*c);

	delete c;
}

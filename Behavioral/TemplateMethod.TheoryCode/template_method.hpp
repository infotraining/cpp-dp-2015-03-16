#ifndef TEMPLATE_METHOD_HPP_
#define TEMPLATE_METHOD_HPP_

#include <iostream>
#include <string>
#include <memory>

class Service
{
public:
    virtual void run()
    {
        std::cout << "Service::run()" << std::endl;
    }

    virtual ~Service() = default;
};

class SuperService : public Service
{
public:
    void run() override
    {
        std::cout << "SuperService::run()" << std::endl;
    }
};

// "AbstractClass"
class AbstractClass
{
protected:
	virtual void primitive_operation_1() = 0;
	virtual void primitive_operation_2() = 0;

    virtual bool is_valid() const
    {
        return true;
    }

    virtual std::unique_ptr<Service> create_service() const
    {
        return std::unique_ptr<Service>(new Service);
    }
public:
	void template_method()
	{
		primitive_operation_1();

        if (is_valid())
            primitive_operation_2();

        std::unique_ptr<Service> srv = create_service();
        srv->run();
		std::cout << std::endl;
	}
	
	virtual ~AbstractClass() {}
};

// "ConcreteClass"
class ConcreteClassA : public AbstractClass
{
protected:
	void primitive_operation_1()
	{
		std::cout << "ConcreteClassA.PrimitiveOperation1()" << std::endl;
	}

	void primitive_operation_2()
	{
		std::cout << "ConcreteClassA.PrimitiveOperation2()" << std::endl;
	}
};

// "ConcreteClass"
class ConcreteClassB : public AbstractClass
{
protected:
	void primitive_operation_1()
	{
		std::cout << "ConcreteClassB.PrimitiveOperation1()" << std::endl;
	}

	void primitive_operation_2()
	{
		std::cout << "ConcreteClassB.PrimitiveOperation2()" << std::endl;
	}

    bool is_valid() const
    {
        return false;
    }

    std::unique_ptr<Service> create_service() const
    {
        return std::unique_ptr<Service>(new SuperService());
    }
};

#endif /*TEMPLATE_METHOD_HPP_*/

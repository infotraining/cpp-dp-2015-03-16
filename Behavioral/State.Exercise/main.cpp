#include "bank_account.hpp"

using namespace std;

NormalState BankAccount::normal_state;
OverdraftState BankAccount::overdraft_state;

AccountState* NormalState::withdraw(AccountContext &context, double amount)
{
    context.balance -= amount;

    if (context.balance < 0)
        return &BankAccount::overdraft_state;

    return this;
}

AccountState* OverdraftState::withdraw(AccountContext &context, double amount)
{
    std::cout << "Brak srodkow na koncie: " << context.id << std::endl;

    return this;
}

AccountState* OverdraftState::deposit(AccountContext &context, double amount)
{
    context.balance += amount;

    if (context.balance >= 0.0)
        return &BankAccount::normal_state;

    return this;
}

int main()
{
	BankAccount ba1(1);
    cout << ba1.status() << endl;
	ba1.deposit(10000.0);
    cout << ba1.status() << endl;
	ba1.withdraw(5000.0);
    cout << ba1.status() << endl;
	ba1.pay_interest();
    cout << ba1.status() << endl;
	ba1.withdraw(10000.0);
    cout << ba1.status() << endl;
	ba1.pay_interest();
    cout << ba1.status() << endl;
	ba1.withdraw(1000.0);
    cout << ba1.status() << endl;
	ba1.deposit(7000.0);
    cout << ba1.status() << endl;
}

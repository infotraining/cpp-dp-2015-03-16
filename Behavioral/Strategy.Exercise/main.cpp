#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <numeric>
#include <fstream>
#include <iterator>
#include <list>
#include <stdexcept>
#include <memory>

struct StatResult
{
	std::string description;
	double value;

	StatResult(const std::string& desc, double val) : description(desc), value(val)
	{
	}
};

typedef std::vector<StatResult> Results;

enum StatisticsType
{
	AVG, MINMAX, SUM
};

class Statistics
{
public:
    virtual void calculate(const std::vector<double>& data, Results& results) = 0;
    virtual ~Statistics() {};
};

class Avg : public Statistics
{
public:
    void calculate(const std::vector<double>& data, Results& results)
    {
        double sum = std::accumulate(data.begin(), data.end(), 0.0);
        double avg = sum / data.size();

        StatResult result("AVG", avg);
        results.push_back(result);
    }
};

class Min: public Statistics
{
public:
    void calculate(const std::vector<double>& data, Results& results)
    {
        double min = *(std::min_element(data.begin(), data.end()));
        results.push_back(StatResult("MIN", min));
    }
};

class Max : public Statistics
{
public:
    void calculate(const std::vector<double>& data, Results& results)
    {
        double max = *(std::max_element(data.begin(), data.end()));

        results.push_back(StatResult("MAX", max));
    }
};


class Sum : public Statistics
{
public:
    void calculate(const std::vector<double>& data, Results& results)
    {
        double sum = std::accumulate(data.begin(), data.end(), 0.0);

        results.push_back(StatResult("SUM", sum));
    }
};

// Comosite
class StatGroup : public Statistics
{
    std::vector<std::shared_ptr<Statistics>> stats_;
public:
    void add(std::shared_ptr<Statistics> stat)
    {
        stats_.push_back(stat);
    }

    void calculate(const std::vector<double>& data, Results& results)
    {
        for(const auto& stat : stats_)
            stat->calculate(data, results);
    }
};

class DataAnalyzer
{
    std::shared_ptr<Statistics> statistics_;
	std::vector<double> data_;
public:
    DataAnalyzer(std::shared_ptr<Statistics> statistics) : statistics_{statistics}
	{
	}

	void load_data(const std::string& file_name)
	{
		data_.clear();

		std::ifstream fin(file_name.c_str());
		if (!fin)
			throw std::runtime_error("File not opened");

		double d;
		while (fin >> d)
		{
			data_.push_back(d);
		}

		std::cout << "File " << file_name << " has been loaded...\n";
	}

	void save_data(const std::string& file_name) const
	{ 
		std::ofstream fout(file_name.c_str());
		if (!fout)
			throw std::runtime_error("File not opened");

		for(std::vector<double>::const_iterator it = data_.begin(); it != data_.end(); ++it)
			fout << (*it) << std::endl;
	}

    void set_statistics(std::shared_ptr<Statistics> statistics)
	{
        statistics_ = statistics;
	}

	void calculate(Results& results)
	{
        statistics_->calculate(data_, results);
	}
};

void print_results(const Results& results)
{
    for(Results::const_iterator it = results.begin(); it != results.end(); ++it)
		std::cout << it->description << " = " << it->value << std::endl;
}

int main()
{
	Results results;

    auto avg = std::make_shared<Avg>();
    auto min = std::make_shared<Min>();
    auto max = std::make_shared<Max>();
    auto minmax = std::make_shared<StatGroup>();
    minmax->add(min);
    minmax->add(max);
    auto sum = std::make_shared<Sum>();

    auto std_statistics = std::make_shared<StatGroup>();
    std_statistics->add(avg);
    std_statistics->add(minmax);
    std_statistics->add(sum);

    DataAnalyzer da(std_statistics);
	da.load_data("data.dat");
	da.calculate(results);

//    da.set_statistics(minmax);
//	da.calculate(results);

//    da.set_statistics(sum);
//	da.calculate(results);

	print_results(results);

	std::cout << "\n\n";

	results.clear();
	da.load_data("new_data.dat");
	da.calculate(results);

	print_results(results);
}

#include "bank_account.hpp"

NormalState BankAccount::NORMAL(0.05);
OverdraftState BankAccount::OVERDRAFT(0.15);

using namespace std;

int main()
{
    BankAccount ba1(1);
    cout << ba1.status() << endl;
    ba1.deposit(10000.0);
    cout << ba1.status() << endl;
    ba1.withdraw(5000.0);
    cout << ba1.status() << endl;
    ba1.pay_interest();
    cout << ba1.status() << endl;
    ba1.withdraw(10000.0);
    cout << ba1.status() << endl;
    ba1.pay_interest();
    cout << ba1.status() << endl;
    ba1.withdraw(1000.0);
    cout << ba1.status() << endl;
    ba1.deposit(7000.0);
    cout << ba1.status() << endl;
}
